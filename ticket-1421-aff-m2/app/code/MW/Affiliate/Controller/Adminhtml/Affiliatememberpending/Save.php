<?php

namespace MW\Affiliate\Controller\Adminhtml\Affiliatememberpending;

use MW\Affiliate\Model\Statusactive;

class Save extends \MW\Affiliate\Controller\Adminhtml\Affiliatememberpending
{
    /**
     * Approve pending Affiliate action
     */
    public function execute()
    {
        $customerId = $this->getRequest()->getParam('id');
        $data = $this->getRequest()->getParams();
        $prefix = $data['link_prefix'];
        if(!$prefix){
            return $this->backWithError();
        }
        if($prefix){
            $customerWithPrefixLink = $this->_affiliatecustomersFactory->create()->getCollection()
                ->addFieldToFilter('link_prefix', $prefix)
                ->addFieldToFilter('customer_id', array('nin' => [$customerId]));
            if($customerWithPrefixLink->getSize() > 0){
                return $this->backWithError();
            }
        }

        if ($customerId) {
            $this->_affiliatecustomersFactory->create()->load($customerId)
                ->setActive(Statusactive::ACTIVE)
                ->setLinkPrefix($prefix)
                ->setCustomerTime(date("Y-m-d H:i:s", (new \DateTime())->getTimestamp()))
                ->save();
            // Re-set referral code for affiliate customers
            $this->_dataHelper->setReferralCode($customerId);

            // Auto assign this member to default group
            $storeId = $this->_customerFactory->create()->load($customerId)->getStoreId();
            $store = $this->_storeFactory->create()->load($storeId);
            $this->_dataHelper->setMemberDefaultGroupAffiliate($customerId, $store->getCode());

            // Send mail when admin approve
            $this->_dataHelper->sendMailCustomerActiveAffiliate($customerId);

            // Set total member customer program
            $this->_dataHelper->setTotalMemberProgram();
            $this->messageManager->addSuccess(__('Total of %1 record(s) were successfully updated', 1));
        }
        $this->_redirect('*/*/');
    }

    public function backWithError(){
        $this->messageManager->addError(__('Can not approve! Please try with an other Unique URL Code'));
        $this->_redirect('*/*/');
    }
}

<?php
namespace MW\Affiliate\Controller\Index;

use Magento\Framework\App\Action\HttpGetActionInterface as HttpGetActionInterface;
use Magento\Sales\Controller\OrderInterface;

class Vieworder extends \Magento\Sales\Controller\AbstractController\View implements OrderInterface, HttpGetActionInterface
{
    /**
     * Order view page
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $orderFactory = $objectManager->create('\Magento\Sales\Model\Order');
        $registry = $objectManager->get('\Magento\Framework\Registry');

        $orderId = (int)$this->_request->getParam('order_id');
        $order = $orderFactory->load($orderId);
        $registry->register('current_order', $order);

        /** @var \Magento\Framework\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        return $resultPage;
    }
}

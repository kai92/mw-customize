<?php
/**
 * NOTICE OF LICENSE
 * The contents of this file are subject to the Lifetime Media Group, Inc. Public License Version 1.1 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy and view the license at
 * https://www.lifetimemediagroup.com/license-agreement
 *
 * DISCLAIMER
 * Redistribution of this code is strictly prohibited and trackable. Each user must obtain legal license to use this software.
 * Please also note: Do not edit or add to this file if you wish to upgrade in the future.
 *
 * @category    Lifetime Media Group POS
 * @package     lifetime_pos
 * @copyright   Copyright (c) 2018 Lifetime Media Group, Inc. (https://www.lifetimemediagroup.com)
 * @license     https://www.lifetimemediagroup.com/license-agreement
 */

namespace MW\Affiliate\Model\ResourceModel;

use Magento\Customer\Ui\Component\DataProvider\Document;
use Magento\Framework\Data\Collection\Db\FetchStrategyInterface as FetchStrategy;
use Magento\Framework\Data\Collection\EntityFactoryInterface as EntityFactory;
use Magento\Framework\Event\ManagerInterface as EventManager;
use Psr\Log\LoggerInterface as Logger;

/**
 * Class UiCollectionAbstract
 * @package MW\Affiliate\Model\ResourceModel
 */
class UiCollectionAbstract extends \Magento\Framework\View\Element\UiComponent\DataProvider\SearchResult
{
    /**
     * @inheritdoc
     */
    protected $document = Document::class;
    protected $mainTable;
    protected $resourceModel;

    protected $localeDate;
    protected $locale;
    protected $dateFormat;
    protected $systemStore;
    protected $priceFormatter;
    protected $request;
    protected $urlBuilder;

    /**
     * Initialize dependencies.
     *
     * @param EntityFactory $entityFactory
     * @param Logger $logger
     * @param FetchStrategy $fetchStrategy
     * @param EventManager $eventManager
     * @param string $mainTable
     * @param string $resourceModel
     */
    public function __construct(
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
        \Magento\Framework\Locale\ResolverInterface $localeResolver,
        $dateFormat = 'M j, Y h:i:s A',

        \Magento\Store\Model\System\Store $systemStore,
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceFormatter,
        \Magento\Framework\App\RequestInterface  $request,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Framework\UrlInterface $urlBuilder,

        EntityFactory $entityFactory,
        Logger $logger,
        FetchStrategy $fetchStrategy,
        EventManager $eventManager
    ) {
        $this->localeDate = $localeDate;
        $this->locale = $localeResolver->getLocale();
        $this->dateFormat = $dateFormat;

        $this->systemStore = $systemStore;
        $this->priceFormatter = $priceFormatter;
        $this->request = $request;
        $this->urlBuilder = $urlBuilder;
        parent::__construct($entityFactory, $logger, $fetchStrategy, $eventManager, $this->mainTable, $this->resourceModel);
    }
}
<?php

namespace MW\Affiliate\Model\ResourceModel\Affiliatecustomers\uiGrid;


class Collection extends \MW\Affiliate\Model\ResourceModel\UiCollectionAbstract
{
    protected  $mainTable = 'mw_affiliate_customers';
    protected  $resourceModel = 'MW\Affiliate\Model\ResourceModel\Affiliatecustomers';

    /**
     * @return mixed
     */
    protected function _initSelect(){
        $select = parent::_initSelect();
        $select->getSelect()
            ->joinLeft(
                array('customer' => $this->getResource()->getTable('customer_entity')),
                'customer.entity_id = main_table.customer_id',
                array(
                    'customer_email' => "customer.email",
                ))
//            ->order(array('main_table.customer_id DESC'))
        ;
        return $select;
    }

    /**
     * @return mixed
     */
    public function getData(){
        $url = $this->urlBuilder->getBaseUrl();
        $data = parent::getData();
        foreach ($data as &$item) {
            $item['link_prefix'] = trim($url) . ($item['link_prefix']);
        }
        return $data;
    }

    public function addFieldToFilter($field, $condition = null)
    {
        if($field == 'customer_email') {
            $field = new \Zend_Db_Expr('customer.email');
        }
        return parent::addFieldToFilter($field, $condition);
    }
}

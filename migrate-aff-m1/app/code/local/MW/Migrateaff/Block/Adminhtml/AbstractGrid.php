<?php

class MW_Migrateaff_Block_Adminhtml_AbstractGrid extends Mage_Adminhtml_Block_Widget_Grid

{
    protected function _prepareCollection()
    {
        $collection = $this->getDataColllection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    public function getDataColllection()
    {
        $collection = MW_Migrateaff_Model_Service::syncService()->getAllAffiliates();
        $collection = $this->modifyCollection($collection);
        return $collection;
    }

    public function modifyCollection($collection)
    {
        return $collection;
    }

    protected function _prepareColumns()
    {
        $this->addColumn('AffiliateID', array(
            'header' => Mage::helper('migrateaff')->__('Old Affiliate ID'),
            'index' => 'AffiliateID',
        ));
        $this->addColumn('LoginName', array(
            'header' => Mage::helper('migrateaff')->__('LoginName'),
            'index' => 'LoginName',
            'filter_condition_callback' => array($this, '_filterNameCallback')
        ));
        $this->modifyColumns();
        $this->addExportType('*/*/exportCsv', Mage::helper('migrateaff')->__('CSV'));
        //Mage::dispatchEvent('prepare_warehouse_stock_columns', array('object' => $this));
        return parent::_prepareColumns();

    }
    /**
     * function to add, remove or modify product grid columns
     *
     * @return $this
     */
    public function modifyColumns()
    {
        return $this;
    }
    /**
     * @return Magestore_Reportsuccess_Model_Service_Inventoryreport_InventoryService
     */
    public function service(){
        return MW_Migrateaff_Model_Service::syncService();
    }

    /**
     * @param $collection
     * @param $column
     */
    public function _filterDebugCallback($collection,$column){
        if (!($value = $column->getFilter()->getValue())) {
            return;
        }
        return  $this->service()->filterDebugCallback($collection,$column->getId(),$value);
    }
    public function _filterNameCallback($collection,$column ){
            if (!($value = $column->getFilter()->getValue())) {
                return;
            }
            return $collection->getSelect()->where('name_table.value like ?', '%'.$value.'%');
    }

}
<?php

class MW_Migrateaff_Helper_Data extends Mage_Core_Helper_Abstract
{
    const LIST_AFF_NOT_SYNC = 'list_aff_not_sync';
    const LIST_TRANSACTION_NOT_SYNC = 'list_transaction_not_sync';

    /**
     *
     * @param string $data
     * @return string
     */
    public function base64Decode($data, $strict = false) {
        return base64_decode($data, $strict);
    }
    /**
     *
     * @param string $data
     * @return string
     */
    public function base64Encode($data) {
        return base64_encode($data);
    }

}
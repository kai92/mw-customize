<?php
$installer = $this;
$collection = Mage::getModel('migrateaff/affcustomer')->getCollection();
$installer->startSetup();
$installer->run("

DROP TABLE IF EXISTS {$collection->getTable('affcustomer')};
CREATE TABLE {$collection->getTable('affcustomer')} (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL default '0',
  `aff_id` int(11) NOT NULL default '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS {$collection->getTable('affrecorded')};
CREATE TABLE {$collection->getTable('affrecorded')} (
  `id` int(11) NOT NULL default '0',
  `aff_id` int(11) NOT NULL default '0',
  `transaction_id` int(11) NOT NULL default '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

$installer->endSetup();

/**
*  customer to affilate account  :  tblaff_Affiliates
*  click count  :  tblaff_ClickCounts
*  click count  :  tblaff_ClicksGeneric
*
*  comission  :  tblaff_Sales_Commissions
*  orders  :  tblaff_Sales
*
*
*/
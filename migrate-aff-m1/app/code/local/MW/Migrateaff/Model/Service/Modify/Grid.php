<?php

use MW_Migrateaff_Helper_Data as Data;
class MW_Migrateaff_Model_Service_Modify_Grid
{

    public function Columns($grid,$type){
        if($type == Data::LIST_AFF_NOT_SYNC){
            return  $this->listAffNotSyncColumns($grid);
        }
    }

    /**
     * @param $grid
     * @return mixed
     */
    public function listAffNotSyncColumns($grid){

        $grid->addColumn('Email', array(
            'header' => $grid->__('Email'),
            'index' => 'Email',
            'sortable' => true,
        ));

        $grid->addColumn('Telephone', array(
            'header' => $grid->__('Telephone'),
            'align' => 'right',
            'width' => '150px',
            'type' => 'number',
            'index' => 'Telephone'
        ));
        return $grid;
    }

}
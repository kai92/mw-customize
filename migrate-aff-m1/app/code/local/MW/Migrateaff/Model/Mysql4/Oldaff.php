<?php

class MW_Migrateaff_Model_Mysql4_Oldaff extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {    
        $this->_init('migrateaff/oldaff', 'AffiliateID');
    }
}
<?php

class MW_Migrateaff_Model_Mysql4_Affcustomer extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {    
        $this->_init('migrateaff/affcustomer', 'id');
    }


    /**
     * @param $table
     */
    public function deleteData($table){
        try {
            $table = Mage::getSingleton('core/resource')->getTableName($table);
            $connection = $this->_getConnection('read');
            $connection->truncate($table);
            return true;
        }catch (Exception $e){
            return false;
        }
    }

    /**
     * @param $table
     * @param $data
     */
    public function insertData($table, $data){
        try{
            $table = Mage::getSingleton('core/resource')->getTableName($table);
            $connection = $this->_getConnection('read');
            $connection->beginTransaction();
            $connection->insertMultiple($table,$data);
            $connection->commit();
            return true;
        }catch (Exception $e){
            $connection->rollBack();
            return false;
        }
    }

    /**
     * @param $tableName
     * @param $select
     * @param bool|false $temporary
     */
    public function createTable($tableName,$select,$temporary = false){
        $connection = $this->_getConnection('read');
        $connection->createTableFromSelect($tableName,$select,$temporary);
    }

    /**
     * @param $tableName
     * @param null $temporary
     */
    public function dropTable($tableName, $temporary = null)
    {
        $connection = $this->_getConnection('read');
        if($temporary){
            $connection->dropTemporaryTable($tableName);
        }else{
            $connection->dropTable($tableName);
        }
    }

    /**
     * @param $data
     * @return bool
     */
    public function updateTable($data){
        $tableinsert = Mage::getSingleton('core/resource')->getTableName('os_debug_movement');
        $table = $data['table'];
        $bin_data = $data['values'];
        $where = $data['condition'];
        $insert = $data['insert'];
        $insert_none_item = $data['insert_none_item_id'];
        try{
            $connection = $this->_getConnection('read');
            $connection->beginTransaction();
            /* insert item_id which not exist in table */
            if($insert_none_item){
                $connection->insertMultiple($table,$insert_none_item);
            }
            /* update item_id that really exist in table */
            if($bin_data) {
                $connection->update($table, $bin_data, $where);
            }
            /* insert history collect qty */
            if($insert) {
                $connection->insertMultiple($tableinsert, $insert);
            }
            $connection->commit();
            return true;
        }catch (Exception $e){
            $connection->rollBack();
        }
    }

}